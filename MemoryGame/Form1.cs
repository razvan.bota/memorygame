﻿using System;
using System.Windows.Forms;

namespace MemoryGame
{
    public partial class Form1 : Form
    {
        PictureBox prev;
        int flag = 0;
        int remain = 8;
        int hint = 3;
        int timeLeft = 60;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Newgame();
        }

        void ResetImages()
        {
            foreach(Control x in this.Controls)
                if(x is PictureBox)
                    (x as PictureBox).Image = Properties.Resources._0;
        }

        void ResetTags()
        {
            foreach (Control x in this.Controls)
                if(x is PictureBox)
                    (x as PictureBox).Tag = "0";
        }

        void ShowImage(PictureBox box)
        {
            switch(Convert.ToInt32(box.Tag))
            {
                case 1:
                    box.Image = Properties.Resources._1;
                    break;
                case 2:
                    box.Image = Properties.Resources._2;
                    break;
                case 3:
                    box.Image = Properties.Resources._3;
                    break;
                case 4:
                    box.Image = Properties.Resources._4;
                    break;
                case 5:
                    box.Image = Properties.Resources._5;
                    break;
                case 6:
                    box.Image = Properties.Resources._6;
                    break;
                case 7:
                    box.Image = Properties.Resources._7;
                    break;
                case 8:
                    box.Image = Properties.Resources._8;
                    break;
                default:
                    box.Image = Properties.Resources._0;
                    break;
            }
        }
 
        void SetTagRandom()
        {
            int[] arr = new int[16];
            int r, index = 0;
            Random rand = new Random();

            while (index < 16)
            {
                r = rand.Next(1, 17);
                if(Array.IndexOf(arr,r)==-1)
                {
                    arr[index] = r;
                    index++;
                }  
            }

            for(index =0; index < 16; index++)
                if (arr[index] > 8)
                    arr[index] -= 8;

            index = 0;
            foreach(Control x in this.Controls)
            {
                if(x is PictureBox)
                {
                    (x as PictureBox).Tag = arr[index].ToString();
                    index++;
                }
            }
        }

        void Compare(PictureBox previous, PictureBox current)
        {
            if(previous.Tag.ToString()==current.Tag.ToString())
            {
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);
                previous.Visible = false;
                current.Visible = false;

                if(--remain==0)
                {
                    timer1.Enabled = false;
                    Hint.Enabled = false;
                    remaining.Text = "Congratulation!";
                    MessageBox.Show("Congratulation!\nPress restart to play again.", "End of game.");
                }
                else
                    remaining.Text = "Image left: " + remain;
            }
            else
            {
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);
                previous.Image = Properties.Resources._0;
                current.Image = Properties.Resources._0;
            }
        }

        void SetPictureBoxVisibilityTrue()
        {
            foreach (Control x in this.Controls)
                if (x is PictureBox)
                    (x as PictureBox).Visible = true;
        }

        void ActiveAll()
        {
            foreach (Control x in this.Controls)
                if (x is PictureBox)
                    (x as PictureBox).Enabled = true;
        }

        void DezactivateAllPictureBox()
        {
            foreach (Control x in this.Controls)
                if (x is PictureBox)
                    (x as PictureBox).Enabled = false;
        }

        void Newgame()
        {
            remain = 8;
            hint = 3;
            SetTagRandom();
            SetPictureBoxVisibilityTrue();
            ResetImages();
            Hint.Enabled = true;
            remaining.Text = "Left image: " + remain;
            Hint.Text = "Hint (" + hint + ")";
            flag = 0;
            timeLeft = 60;
            time.Text = "Remaining time: " + timeLeft;
            timer1.Enabled = true;
            ActiveAll();

        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            PictureBox current = (sender as PictureBox);
            ShowImage((sender as PictureBox));
            if (flag == 0)
            {
                prev = current;
                flag = 1;
            }
            else 
                if(prev!=current)
                {
                    Compare(prev, current);
                    flag = 0;
                }
        }

        private void Hint_Click(object sender, EventArgs e)
        {
            foreach(Control x in this.Controls) if(x is PictureBox) ShowImage((x as PictureBox));
            Application.DoEvents();
            System.Threading.Thread.Sleep(1500);
            ResetImages();
            if (--hint == 0)
                Hint.Enabled = false;
            Hint.Text = "Hint (" + hint + ")";
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (--timeLeft == 0)
            {
                timer1.Enabled = !timer1.Enabled;
                time.Text = "Time is over.";
                MessageBox.Show("The time is over!", "End of game!");
                DezactivateAllPictureBox();
                Hint.Enabled = false;
                
            }
            else
                time.Text = "Remaining time: " + timeLeft;
        }

        private void NewGameButton_Click(object sender, EventArgs e)
        {
            Newgame();
        }

        private void ReturnMenu_Click(object sender, EventArgs e)
        {

            Form2 form = new Form2();
            form.Show();
            this.Close();

        }
    }
}
